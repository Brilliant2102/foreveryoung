﻿<!doctype html><!--nav바를 넣을 수 있는 ver-->
<html>
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>
		</title>
		<!-- 합쳐지고 최소화된 최신 CSS -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
		<style>

		
			 table.type04 {
			  border-collapse: separate;
			  border-spacing: 1px;
			  text-align: left;
			  line-height: 1.5;
			  border-top: 1px solid #ccc;
			  margin : 20px 10px;
		   }
		   table.type04 th {
			  width: 150px;
			  padding: 10px;
			  padding-left:30px;
			  font-weight: bold;
			  vertical-align: top;
			  border-bottom: 1px solid #ccc;
		   }
		   table.type04 td {
			  width: 1000px;
			  padding: 10px;
			  vertical-align: top;
			  border-bottom: 1px solid #ccc;
		   }
		   .title_img{
				padding-top:30px
		   }
		   .title_bener{
				padding:30px;
		   }
		</style>
		
		


	</head>

	<body>
		<!--규칙 큰 항목 별은 br을 2개쓰고 작은 글씨 사이에서는 br을 한번 네모칸안에 있는 아이들끼리는 br을 안씀-->
		<div class = " container-fluids">
			<!--하단에 고정된 구매하기 버튼-->
			<nav class="navbar fixed-bottom navbar-light bg-light" style="width:100%;margin:0 auto">
				<div class="container-fluids" >
					<a class="navbar-brand" href="#">Fixed bottom</a>
				</div>
			</nav>
		<div>

		<div class = " container">
			<div class = " row justify-content-center ">
	<!--사진과 설명이 있는 베너-->
				<div class = " col-xs-12 col-sm-12 col-md-12 col-lg-6 " style = " background-color:none;padding:30px;padding-bottom:0px;margin-left:0px">
					<div><img src="http://101.101.218.121/내 맘대로 홈페이지 만들기.png" width="100%" height="100%" style="margin:0 auto"></div>
				</div>
				<div class = "col-xs-12 col-sm-12 col-md-12 col-lg-6 " style="background-color:#none;">
					<div class = "title_bener">
						<div style="padding:10px;padding-left:20px;padding-right:20px;background-color:#f0f0f0;border-radius:10px 10px 0px 0px"> 
							<h6><strong>#웹개발자_필수_강의 #나만의_홈페이지만들기 <br>#너도_웹개발자_할_수_있어!</strong></h6>
						</div>
						<div style="padding:10px">
							<br>
							<h4><strong>내맘대로 홈페이지 만들기</strong></h4>
							<h6><strong>: 웹페이지를 내맘대로 꾸미는 방법을 알려드립니다!</strong></h6>
							<br>
							<h6><strong>주 1회 수업:</strong> 00000원</h6>
							<h6><strong>주 2회 수업: 00000원</strong></h6>
							<br>
						</div>
						<div class=" row justify-content-center " style="background-color:#f0f0f0;width:100%;margin:0 auto;padding:10px">
							<label style="text-align:center"><a href="#" >수강결제하기</a></label>
						
						</div>
	
						
					</div>
									
				</div>
				
				<hr>
	<!--강의 소개글-->					
			<div class="row justify-content-center">
			
				<div class="col-auto">		
					<br><br><br><br><br>
					<!--밑줄 조금 띄어지게 다시할 것-->
					<h4 style="text-decoration:underline;"><strong>강의소개</strong></h4>
				</div>
				
			</div>
	<!--반드시 필요한 선수지식-->
			<div class="row justify-content-center">
			
				<div class="col-auto">
					<br><br><br>
					<h4><strong>반드시 필요한 선수지식</strong></h4>
					<br>
				</div>
				
			</div>
		<!--부가 설명-->
			<div class="row justify-content-center">
			
				<div class="col-auto">
					<h6><strong>반드시 아래 지식을 가지고 있어야 가능합니다.</strong></h6>
					<br>
				</div>
				
            </div>
		<!--상세 설명-->
			<div class="row justify-content-center">
			
				<div class=" col-sm-0 col-md-0 col-lg-2" style="background-color:none;">
				</div>
				
				<div class=" col-sm-12-auto col-md-10-auto col-lg-8" style="background-color:#f0f0f0;padding:30px">
					<h5 style="text-align:center">
						<strong>HTML,CSS,자바스크립트에 대한 지식을 갖추고 있어야 합니다.</strong>
					</h5>
					<h6  style="text-align:center" >
						<strong>본 강좌는 "웹" 애플리케이션을 개발합니다. <br>따라서 위 세가지 기술에 대한 이해가 없이는 본 강좌를 학습하는 것이 불가능합니다.</strong>
					</h6>
				</div>
				
				<div class="col-sm-0 col-md-0 col-lg-2" style="background-color:none;">
				</div>
				
            </div>
	<!--이 강의에서 배우는 것 들-->
			<div class="row justify-content-center">
				<div class="col-auto">
					<br><br><br>
					<h4><strong>이 강의에서 배우는 것들</strong></h4>
					<br>
				</div>
			</div>
		<!--부가 설명-->
			<div class="row justify-content-center">
				<div class="col-auto">
					<h6><strong>대부분의 웹 애플리케이션이 기본으로 갖추고 있는 기능을 구현</strong></h6>
					<br>
				</div>
            </div>
		<!--상세 설명-->
			<div class="row justify-content-center">
			
				<div class=" col-sm-0 col-md-0 col-lg-2" style="background-color:none;">
				</div>
				
				<div class=" col-sm-12-auto col-md-10-auto col-lg-8" style="background-color:#f0f0f0;padding:30px;padding-left:50px;padding-right:50px">
					<h6 style="text-align:left">
						<strong>1. 회원가입, 로그인, 로그아웃 등 홈페이지의 기본기능을 배웁니다.</strong>
					</h6>
					<h6 style="text-align:left"><strong>2. 예외처리기능에 대해 배웁니다</strong></h6>
					<h6 style="text-align:left"><strong>3. 예외처리기능에 대해 배웁니다</strong></h6>
				</div>
				
				<div class="col-sm-0 col-md-0 col-lg-2" style="background-color:none;">
				</div>
				
            </div>

			<!--네모 박스안에 글씨쓰기-->
			
<!--이 강의에서 다루는 툴-->
			
			<div class="row justify-content-center">
			
				<div class="col-auto">
				<br><br><br>
					<h4><strong>이 강의에서 다루는 툴</strong></h4>
				</div>
				
			</div>
			<br>
			<div class="row justify-content-center">
				<div class="col-auto" style="text-align:center">
					<h6><strong>인텔리J IDEA, 부트스트랩, 제이쿼리, 타임리프스프링<br>스프링 부트, 스프링 데이터 JPA, 스프링 시큐리티JPA, QueryDSL, PostgreSQL, JUnit</strong></h6>
				</div>
            </div>
			<br>
			<div class="row justify-content-center"><!--툴사진이 한줄에 기본 4개가 들-->
				<div class="col-sm-0 col-lg-2" style="background-color:#d6d6d6;">
					
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-0 col-lg-2" style="background-color:#d6d6d6;">
					
				</div>
			</div>
			
			<!--상세커리큘럼-->
			<div class="row justify-content-center">
				<div class="col-auto">
					<br><br><br>
					<h4><strong>상세 커리큘럼</strong></h4>
					<br>
				</div>
			</div>
			<br>
			
			<div class="row justify-content-center">
				<div class="col-auto" style="text-align:center">
					<h6><strong>이 과목은 이런 커리큘럼으로 진행됩니다</strong></h6>
					<br>
				</div>
            </div>  
			
			<div class="row justify-content-center">
				<div class="col-sm-0 col-md-1 col-lg-2"></div>
				<div class="col-sm-12 col-md-10 col-lg-8">
					<div class="accordion" id="accordionExample">
						<div class="accordion-item">
							<h2 class="accordion-header" id="headingOne">
								<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									서버구축
								</button>
							</h2>
							<div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
								<div class="accordion-body">
									 <table class="type04" style="margin:0 auto">
                           <tr>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                         <tr>
                               
                              <td>내용이 들어갑니다.</td>
                           </tr><tr>
                             
                              <td>내용이 들어갑니다.</td>
                           </tr><tr>
                             
                              <td>내용이 들어갑니다.</td>
                           </tr>
                        </table>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headingTwo">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									홈페이지 만들기
								</button>
							</h2>
							<div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
								<div class="accordion-body">
									<strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headingThree">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									Accordion Item #3
								</button>
							</h2>
							<div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
								<div class="accordion-body">
									<strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
								</div>
							</div>
						</div>
					</div>
				</div> 
				<div class="col-sm-0 col-md-1 col-lg-2"></div>
			</div>

		<!--FAQ-->
			<div class="row justify-content-center">
				<div class="col-auto">
					<br><br><br>
					<h4><strong>FAQ</strong></h4>
					<br>
				</div>
			</div>
			<br>
			
			<div class="row justify-content-center">
				<div class="col-auto" style="text-align:center">
					<h6><strong>많은 수강생들이 궁금해했던 질문입니다.</strong></h6>
					<br>
				</div>
            </div>  
			<br>
			<div class="row justify-content-center">
				<div class="col-sm-0 col-md-1 col-lg-2"></div>
				<div class="col-sm-12 col-md-10 col-lg-8">
					<div class="accordion accordion-flush" id="accordionFlushExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-headingOne">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
        Accordion Item #1
      </button>
    </h2>
    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
        Accordion Item #2
      </button>
    </h2>
    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="flush-headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
        Accordion Item #3
      </button>
    </h2>
    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
    </div>
  </div>
</div>
</div>
				<div class="col-sm-0 col-md-1 col-lg-2"></div>
			</div>
			<!--지니 요구사항 다른 페이지로 탭하기-->
			<div class ="row"><a href="#">더 많은 질문을 보려면</a></div>
			</div>
			
			
		</div>
	</body>
</html>