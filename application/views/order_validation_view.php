﻿<!--
# 주문 정보를 받는 View를 만들어야 합니다
 
1. input을 통해 상품의 이름을 입력받을 수 있어야 합니다.
2. select를 통해 상품의 개수를 1개에서 5개중 하나를 선택할 수 있어야 합니다.
3. input을 통해 주문자 이름을 입력할 수 있어야 합니다.
4. input을 통해 주문자 이메일을 입력할 수 있어야 합니다.
5. input을 통해 주소를 입력할 수 있어야 합니다.
6. 위 정보를 form으로 전송해야 합니다.-->
<html>
	<head>
		<title>
			주문정보
		</title>

	</head>
	<body>
		<form action="http://101.101.218.121/validation_controller/check" method="post">
			원하는 상품명
			<input type="text" name="product_name" required>
			
			수량
			<select name ="quantity">
						<option value ="1개">1개</option>
						<option value ="2개">2개</option>
						<option value ="3개">3개</option>
						<option value ="4개">4개</option>
						<option value ="5개">5개</option>
			</select>
			<br>
			<br>
			주문자이름
			<input type="text" name="order_name"><br>
			
			이메일
			<input type="text" name="email"><br>
			
			주소
			<input type="text" name="address">
			
			<button type="submit">전송</button>
		</form>
	</body>