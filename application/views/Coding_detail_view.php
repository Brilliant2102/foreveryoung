﻿<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <title>CSS</title>
    <style>   
   a{
      color:#000;
      text-decoration:none;
   }
   a:link{ 
   
   }
   
   a:visited{
      color:#000;
   }
    #main_page{
        width: 1300px;
        margin: 0px auto;
        padding: 20px;
    }
   
   .box1{
      float:left;
      width:60%;
      height:400px;
      background-color:#d0d0d0;
   }
   .box2{
      display:inline-block;
      width:40%;
      height:400px;
      background-color:#e6e6e6;
   }
   .box2 > h1{
      padding-left:30px;
      padding-right:30px;
      line-height:1.3em
   }
   .box2 > h2{
      padding-left:30px;
      line-height:1.3em
   }
   .box2 > h3{
      padding-left:30px;
      line-height:1.3em
   }
   .box2 > h4{
      padding-left:30px;   
      line-height:1.3em
   }
   .explain1{
      width:100%;
      
      padding:10px;
      background-color:yellow;
   }
   .box4{
      float:left;
      width:40%;
      height:230px;
      background-color:#d0d0d0;
   }
   .box5{
      display:inline-block;
      width:60%;
      height:230px;
      background-color:#e6e6e6;
   }
   .box5 > h2{
      padding-left:30px;
   }
   .box5 > h3{
      padding-left:30px;
   }
   .box5 > h4{
      padding-left:30px;
   }
   
    #content {
        width: 100%;
      margin: 0 auto;
        margin-bottom: 10%;
        float: left;
      display: block;
      height:30px;
      
    }
    .detail_menubar{
      display:inline-block;
      text-align:center;
      padding:10px;
      background-color:#f0f0f0;
      width:100%;
      margin:20px;<!--메뉴바 틀자체의 margin-->
   }
   .detail_menubar > span{
      padding-left:20px;
      padding-right:20px;
      display:inline-block;
      width:100px;
      height:20px;
   }
   .detail_menubar > span > a{
      font-size: 15pt;
      font-weight:bold;
   }
   .detail_menubar > span > a:hover{
      text-decoration:underline;
      text-underline-position:under;
      
   }
   .curiculum>div{
      width:500px;
      padding:10px;
      padding-left:20px;
      margin:10px;
      margin-left:50px;
      background-color:yellow;
   }
   .curiculum > h2{
      text-align:center;
   }
   .curiculum > h4{
      text-align:center;
   }
  
   .intro>h5{
      padding-left:50px;
   }
   .menu {
   list-style:none;
      width:70%;
      padding:10px;
      padding-left:30px;
      padding-right:30px;
      margin:15px auto;
      background-color:#d0d0d0;
   }
   .menu a{
      cursor:pointer;
   
   }
   .menu .hide {
   list-style:none;
      display:none;
      margin:0px;
      margin-top:10px;
      background-color:white; 
      padding:10px;
   }
   .detail_content{
      padding-left:200px;
      padding-right:200px;
   }
   .intro > h2{
      text-align:center;
   }
   table.type04 {
      border-collapse: separate;
      border-spacing: 1px;
      text-align: left;
      line-height: 1.5;
      border-top: 1px solid #ccc;
      margin : 20px 10px;
   }
   table.type04 th {
      width: 150px;
      padding: 10px;
      padding-left:30px;
      font-weight: bold;
      vertical-align: top;
      border-bottom: 1px solid #ccc;
   }
   table.type04 td {
      width: 1000px;
      padding: 10px;
      vertical-align: top;
      border-bottom: 1px solid #ccc;
   }
   
   .pay_box{
      text-align:center; 
      background-color:#d6d6d6;
      padding:20px;
      margin:25px;
      
      
   }
   #weekly_plan > h2{
      text-align:center;
   }
   #weekly_plan > h3{
      text-align:center;
   }
   #weekly_plan > h4{
      text-align:center;
   }
   .basic_info > div{
		background-color:#f0f0f0;
		width:70%;
		text-align:center;
		margin:15px auto;
		padding:10px;		
	}
	basic_info > div > h4{
		ext-align:left;
		padding-left:40px;
		padding-right:40px;
		padding-top:0px;
		padding-buttom:10px		
	}
   .basic_info > h2{
      text-align:center;
   }
   .basic_info > h3{
      text-align:center;
   }
   .basic_info > h4{
      text-align:center;
   }
   .menu_c { 
      list-style:none;
      width:70%;
      padding:10px;
      padding-left:30px;
      padding-right:30px;
      margin:15px auto;
      background-color:#d0d0d0;
   }
   .menu_c a{
      cursor:pointer;
   }
   .menu_c .hide {
      list-style:none;
      display:none;
      margin:0px;
      margin-top:10px;
      background-color:white; 
      padding:10px;
   }
   #tool_info{
      text-align:center;
   }
   #tool_info > div > div{
      display:inline-block;
      vertical-align:top;
      margin:10px auto;
      margin-top:0px
   }
   #tool_info > div > div>div.tool_img{
      background-color:#f0f0f0;
      width:150px;
      height:150px;
      text-align:center;
      margin:0 auto;
      padding:10px;
      display-inline:block;
   }
   #tool_info > div > div>div.tool_name{
      background-color:#d6d6d6;
      width:150px;
      text-align:center;
      margin:0 auto;
      margin-buttom:10px;
      padding:10px;
      display-inline:block;
   }
   #FAQ_style > h2{
      text-align:center;
   }
   #FAQ_style > h3{
      text-align:center;
   }
   #FAQ_style > h4{
      text-align:center;
   }
   #learn_info{
      text-align:center;
   }
   #learn_info > div{
      background-color:#f0f0f0;
      width:80%;
      text-align:center;
      margin:15px auto;
      padding:10px;
   }
   #learn_info > div > h3{
      text-align:left;
      padding-left:40px;
      padding-right:40px;
      padding-top:0px;
      padding-buttom:10px;
   }
   #learn_info > div > h4{
      text-align:left;
      padding-left:40px;
      padding-right:40px;
      padding-top:0px;
      padding-buttom:10px;
   }

   </style>
   <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
   <script>
      $(document).ready(function(){
         $(".menu>a").click(function(){
            var submenu = $(this).next("ul");
            if( submenu.is(":visible") ){
               submenu.slideUp();
            }
            else{
               submenu.slideDown();
            }
         });
      });
     $(document).ready(function(){
         $(".menu_c>a").click(function(){
            var submenu = $(this).next("ul");
            if( submenu.is(":visible") ){
               submenu.slideUp();
            }
            else{
               submenu.slideDown();
            }
         });
      });
</script>
  </head>
  <body>
    <div id="main_page">

<!--여기부터 내가-->
      <div id="main_title">

         <div class ="box1">
            <img src="#">
         </div>
         
         <div class ="box2">
            <h1> 웹개발자 </h1>
            <!--여기는 수정 후 다시-->
            <div style="padding:10px;margin-left:20px;margin-bottom:0px"><strong>강의 설명</strong></div>
            <div style="background-color:#d6d6d6;width:85%;padding:10px;padding-left:20px;padding-right:20px;margin: 0 auto">
               <div>dddddddddddddd</div><!--줄이 자동으로 안넘어가-->
            </div>
            <div style="padding:10px;margin-left:20px;margin-bottom:0px"><strong>수강료</strong></div>
            <div style="background-color:#d6d6d6;width:85%;padding:10px;padding-left:20px;margin: 0 auto">
               <span>주 1회:0000000</span><span>, 주 2회:0000000</span>
               
            </div>
            <div class="pay_box"><a href ="#"><strong>수강결제하기</strong></a></div>
         </div>
         
      </div>
     <!--세부 강의 설명이 들어갈 부분-->
      <div class="detail_content">
         <div id="content" style="position:static">
            <div class="detail_menubar"><!---메뉴바 스크롤 바로 내리는 거 만들기-->
               <span><a href="#1">강의소개</a></span>
               <span><a href="#2">커리큘럼</a></span>
               <span><a href="#3">시간표</a></span>
               <span><a href="#4">FAQ</a></span>
            </div>
            <a name="target"></a>
         </div>
         
         <a name="1" class ="intro"><!--강의소개 부분-->
            <h2 style="text-decoration:underline;">강의소개</h2>   
         </a><br><br>
         
         <a name="#" class ="basic_info"><!--강의소개 부분-->
            <h2>반드시 필요한 선수지식</h2>
            <h4>반드시 아래 지식을 가지고 있어야 가능합니다.</h4>
            <div>
               <h3>HTML,CSS,자바스크립트에 대한 지식을 갖추고 있어야 합니다.</h3>
               <h4>본 강좌는 "웹" 애플리케이션을 개발합니다. <br>따라서 위 세가지 기술에 대한 이해가 없이는 본 강좌를 학습하는 것이 불가능합니다.</h4>
            </div>
            
            <div>
               <h3>자바, 스프링,JPA를 학습한 경험이 있어야합니다</h3>
               <h4>완멱히 알고 있진 않아도 괜찮지만, 처음 학습하시는 분들에게 이 강좌는 그저 따라하니까 되네? 정도에 그칠 겁니다. 의미없는 학습입니다.
					반드시 인터넷 강좌나 책으로 한번 쯤 학습해 본 개발자 또는 학생만 이 강좌를 수강해보시길 바랍니다.</h4>      
			</div>
         </a><br><br>
         <!--이 강의에서 배우는 것들, 어떤식으로 가야할지 잘모르겠어,사진-말, 말,-->
         <a name="#" id ="learn_info"><!--강의에서 배우는 정보가 무엇인지 부분-->
            <h2>이 강의에서 배우는 것들</h2>
            <h4>대부분의 웹 애플리케이션이 기본으로 갖추고 있는 기능을 구현</h4>
            <div>
               <!--<h3>1. 회원가입, 로그인, 로그아웃등 홈페이지에서 기본적으로 갖추는 기능에 대해 배웁니다</h3>-->
               <h3>1. 회원가입, 로그인, 로그아웃 등 홈페이지의 기본기능을 배웁니다</h3>
               <h3>2. 도메인 테이터 CRUD에 대해 배웁니다</h3>
               <h3>3. 예외처리기능에 대해 배웁니다</h3>
            </div>
      
         </a><br><br>
         
         <!--이 강의에서 사용하는 툴-->
         <a name="#" id ="tool_info"><!--강의소개 부분-->
            <h2>이 강의에서 다루는 툴</h2>
            <h4>인텔리J IDEA, 부트스트랩, 제이쿼리, 타임리프스프링<br> 스프링 부트, 스프링 데이터 JPA, 스프링 시큐리티JPA, QueryDSL, PostgreSQL, JUnit </h4>
            <div><!--한 줄에 최대 5개, 넘어가면 다음줄에 출력-->
               <div>
                  <div class="tool_img">툴사진</div>
                  <div class="tool_name">인텔리J IDEA</div>
               </div>
               <div>
                  <div class="tool_img">툴사진</div>
                  <div class="tool_name">툴이름</div>
               </div>
               <div>
                  <div class="tool_img">툴사진</div>
                  <div class="tool_name">툴이름</div>
               </div>
               <div>
                  <div class="tool_img">툴사진</div>
                  <div class="tool_name">툴이름</div>
               </div>
         
            </div>
         </a><br><br>
         
         <a name="2" class="curiculum"><!--커리큘럼설명 부분-->
            <h2>상세 커리큘럼</h2>
            <h4>이 수업은 이런 커리큘럼으로 수업이 진행됩니다!</h4>
  
               <li class="menu_c">
                  <a>홈페이지 만들기</a>
                  <ul class="hide"> <!--펼치면 커리큘럼 나오게 하기-->
                     <li>
                        <table class="type04" style="margin:0 auto">
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                        </table>
                     </li>
                  </ul>
   
               </li>
               
               <li class="menu_c">
                  <a>서버구축</a>
                  <ul class="hide"> <!--펼치면 커리큘럼 나오게 하기-->
                     <li>
                        <table class="type04" style="margin:0 auto">
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                           <tr>
                              <th scope="row">항목명</th>
                              <td>내용이 들어갑니다.</td>
                           </tr>
                        </table>
                     </li>
                  </ul>
   
               </li>

         </a><br><br>
  
         <a name="3" id="weekly_plan">
            <h2>시간표</h2>
            <h4>이시간에 선택해서 수업을 들을 수 있어요!</h4>
            <div style="background-color:#d6d6d6;width:100%;height:400px"></div><!--시간표이미지등이 들어갈 공간-->
         </a><br>
         
         <a name="4" id="FAQ_style">
            <h2>FAQ</h2>
            <h4>많은 수강생들이 질문했던 내용입니다!</h4>
            
               <li class="menu">
                  <a>Q. 비전공자들도 들을 수 있나요?</a>
                  <ul class="hide">
                     <li>A. 답변을 입력합니다</li>
                  </ul>
                  
            
               </li>

               <li class="menu"> 
                  <a>Q. 이 강의만의 특별한 장점이 있을까요?</a>
                  <ul class="hide">
                    <li>A. 답변</li>
                  </ul>
                  
               </li>
               
               <li class="menu"> 
                  <a>Q. 이 강의만의 특별한 장점이 있을까요?</a>
                  <ul class="hide">
                    <li>A. 답</li>
                  </ul>
                  
               </li>
         </a><br><br>
             
      </div> 
   </div>
   
  </body>
</html>