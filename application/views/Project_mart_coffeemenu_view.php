﻿<html>
	<head>	
		<title>
			커피/음료
		</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<style>
			body{
				padding-left:5%;
				padding-right:5%;
				padding-top:0%;
								
			}
			
			.header{
				text-align:center; 
			}
			
			.footer{
				display:flex;
				justify-content:center;
				align-items:center;
				height:15%;
			
			}
			
			.mainpage{
				padding-left:30px;
				padding-right:30px;

			}
			.div_recommend{
				background-color:none;
				display:inline-block;
			}
			
			.span_element{
				display:inline-block;
				justify-content:center;
				align-items:center;
				width:320px;
				height:380px;
				background-color:none;
				margin-top:10px;
				padding:10px;
				text-align:center;
				margin-right:15px;
				vertical-align: top; <!--최상단에 줄맞추기위한 -->
			
			}
			.span_element:hover{
				background-color:#eee;
				box-shadow:2px 2px 2px 2px #eee
			}
			.span_element2{
				display:inline-block;
				justify-content:center;
				align-items:center;
				width:420px;
				height:520px;
				background-color:#eee;
				margin-top:10px;
				margin-right:40px;
				padding:10px;
				vertical-align: top; <!--최상단에 줄맞추기위한 -->
			}
			.span_element2:hover{
				background-color:skyblue;
			}
			.span_element3{
				display:inline-block;
				justify-content:center;
				align-items:center;
				width:200px;
				height:200px;
				background-color:orange;
				margin-top:10px;
				text-align:center;
				vertical-align: top; <!--최상단에 줄맞추기위한 -->
			}
			
					#menubar ul{
				margin:0;
				
			}
		
			#menubar ul li{
				display:inline-block;
				list-style-type:none;
				padding-top:1%;
				padding-bottom:1%;
				padding-left:2%;
				padding-right:2%;
				margin:0%;

			 }
			#menubar1 ul li{
				display:inline-block;
				list-style-type:none;
				padding-top:1%;
				padding-bottom:1%;
				padding-left:0.8%;
				padding-right:0.8%;
				margin:0%;

			 }
			 
			 #menubar1 ul li a{
				margin:0;
				color:black;
				
			}
			
			 #menubar ul li a{
				color:BLACK; <!--보이는 색깔-->
				text-decoration:none;<!--마우스를 올렸을때 밑줄을 그을 껀지 아닐지 디폴트는 그음-->
				
			 }
			 #menubar ul li a:hover{
				color:violet;
				<!--글자가 진하게 보였으면 좋겠어-->
			 }
		
			 <!--드랍다운 공부-->
		    ul, ol, li { list-style:none; margin:0; padding:0; }
		   
		
			ul.myMenu {}
			ul.myMenu > li { 
				display:inline-block;  
				background:#eee; 
				border:1px solid #eee; 
				text-align:center; 
				position:relative; 
			}
			
			ul.myMenu > li:hover { 
				background:#fff; 
			}
				
			ul.myMenu > li ul.submenu { 
				display:none; 
				position:absolute; 
				top:30px; 
				left:0; 
			}
			
			ul.myMenu > li:hover ul.submenu { 
				display:block; 
			}
			
			ul.myMenu > li ul.submenu > li { 
				display:inline-block; 
				width:100%; 
				padding:5px 10px; 
				background:#eee; 
				border:1px solid #eee; 
				text-align:center; 
			}
			ul.myMenu > li ul.submenu > li:hover { 
			background:#fff; }
			</style>
			<script src ="https://code.jquery.com/jquery-3.3.1.min.js"></script>
			<script>
			function login(){ 
				//alert("ee"); return false;
				$.post(
				"http://101.101.218.121/project_mart_controller/loginCheck",
				{id:document.getElementById("id").value,
				pw:document.getElementById("pw").value
				},
				function(data){
					if(data=="입력하신 정보와 일치하는 회원정보가 없습니다")
						alert(data);
					else{
						
					} 
						
				}
				);
			}
			</script>
	</head>
	<body>
		<p>
			<div class="header">
				<p><span style="font-size:3em;background-color:none;"><a href="http://101.101.218.121/project_mart_controller/start" style="text-decoration:none;color:black"><strong>커피장사</strong></a></span></p>
				<span style="font-size:1em;background-color:none;padding-right:10%;padding-left:10%; padding-top:0.3%;padding-botton:0.3%">
					커피 맛과 향, 커피장수에 의해 결정되노라
				</span>
			</div>
		</p>
		<hr>
		
		<div id="menubar" style="font-size:1.2em; text-align:left padding-left:0;background-color:none">
			<ul class="myMenu" style="font-align:center;background-color:#eee" >
					<li class="menu1"><a href="http://101.101.218.121/project_mart_controller/start"><strong>HOME</strong></a></li>
					
					<li class="menu2"><a href="http://101.101.218.121/project_mart_controller/coffeemenu">커피 and 음료</a></li>
					
					
					
					<!--<li class="menu3"><a href="http://101.101.218.121/project_mart_controller/drinkmenu">음료</a>
							<ul class="menu3_s submenu">
								<li><a href="#">에이드</a></li>
							</ul>
					</li>-->
						
						
					<li class="menu4"><a href="http://101.101.218.121/project_mart_controller/breadmenu">디저트</a></li>
					<li class="menu5"><a href="http://101.101.218.121/project_mart_controller/cakemenu">케이크</a></li>
					<li><a href="http://101.101.218.121/project_mart_controller/load_view">오시는 길</a></li>
			</ul>
		
		</div>
	</div>
		<br>
	
			<div style="text-align:center;font-size:2.7em">커피/음료 코너<br></div>
			<div class="mainpage"><hr>
			
			<!--사진 슬라이드넣기-->
				<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
				
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="padding-top:5px">
					  <ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					  </ol>
					  <div class="carousel-inner">
						<div class="carousel-item active">
							<img src="http://101.101.218.121/191104_1_140163.jpg" class="d-block w-100" alt="사진 넣었다 치고" height="350">
						</div>
						<div class="carousel-item">
						  <img src="http://101.101.218.121/빵.jpg" class="d-block w-100" alt="..." height="350">
						</div>
						<div class="carousel-item">
						  <img src="http://101.101.218.121/200507_카라멜마끼야또-아이스-1280x1280.jpg" class="d-block w-100" alt="..." height="350"> 
						</div>
					  </div>
					  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					  </a>

				</div>
			<!--추천 메뉴 넣기-->
			<div style="background-color:none; margin-top:30px">
				<span style="background-color:none;margin-left:10px;font-size:2em">BEST 커피/음료</span><br><hr>
			</div>
			
			<div class="div_recommend">
				<span class="span_element" style="margin-left:15px">
					<div><img src="http://101.101.218.121/아아.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Ice아메리카노</strong></div>
					<div style="padding-top:4px">4,100원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/12.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>마시멜로우 핫초코</strong></div>
					<div style="padding-top:4px">3,900원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/200507_카라멜마끼야또-아이스-1280x1280.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Ice 카라멜 마끼야또</strong></div>
					<div style="padding-top:4px">5,600원</div>
				</span>
			
				<span class="span_element">
					<div><img src="http://101.101.218.121/13.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong> 아이스초코</strong></div>
					<div style="padding-top:4px">5,500원</div>
				</span>
			
			
			</div>
			<!--전체 메뉴 넣기-->
			<div style="background-color:none; margin-top:30px">
				<span style="background-color:none;margin-left:10px;font-size:2em">전체 커피/음료 메뉴</span><br><hr>
			</div>
			
			<div class="div_recommend">
				<span class="span_element" style="margin-left:15px">
					<div><img src="http://101.101.218.121/아아.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Ice아메리카노</strong></div>
					<div style="padding-top:4px">4,100원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/12.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>마시멜로우 핫초코</strong></div>
					<div style="padding-top:4px">3,900원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/200507_카라멜마끼야또-아이스-1280x1280.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Ice 카라멜 마끼야또</strong></div>
					<div style="padding-top:4px">5,600원</div>
				</span>
			
				<span class="span_element">
					<div><img src="http://101.101.218.121/13.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong> 아이스초코</strong></div>
					<div style="padding-top:4px">5,500원</div>
				</span>
			
			
			</div>
			
				<div class="div_recommend">
				<span class="span_element" style="margin-left:15px">
					<div><img src="http://101.101.218.121/14.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Hot 달고나라뗴</strong></div>
					<div style="padding-top:4px">4,100원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/15.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Ice 라떼</strong></div>
					<div style="padding-top:4px">3,900원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/16.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Hot 라떼</strong></div>
					<div style="padding-top:4px">5,600원</div>
				</span>
			
				<span class="span_element">
					<div><img src="http://101.101.218.121/17.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Ice 바닐라라떼</strong></div>
					<div style="padding-top:4px">5,500원</div>
				</span>
			
			
			</div>
			
			
				<div class="div_recommend">
				<span class="span_element" style="margin-left:15px">
					<div><img src="http://101.101.218.121/18.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Hot 바닐라라떼</strong></div>
					<div style="padding-top:4px">4,100원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/19.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Hot 히비스커스</strong></div>
					<div style="padding-top:4px">3,900원</div>
				</span>
				<span class="span_element">
					<div><img src="http://101.101.218.121/20.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Hot 청귤차</strong></div>
					<div style="padding-top:4px">5,600원</div>
				</span>
			
				<span class="span_element">
					<div><img src="http://101.101.218.121/21.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong> Ice 자색고구마라떼</strong></div>
					<div style="padding-top:4px">5,500원</div>
				</span>
			
			
			</div>
			
			
				<div class="div_recommend">
				<span class="span_element" style="margin-left:15px">
					<div><img src="http://101.101.218.121/22.jpg" width=300 height =300></div>
					<div style="font-size:1.1em;padding-top:7px"><strong>Hot 자색고구마라떼</strong></div>
					<div style="padding-top:4px">4,100원</div>
				</span>
		
			
			
			</div>
			
			
			</div>
			
					<hr>
		<div class="footer" style="background-color:none;">커피장사를 찾아주셔셔 감사합니다</div>
	</div>

	</body>
</html>