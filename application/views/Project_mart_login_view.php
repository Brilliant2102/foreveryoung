﻿<html>
	<head>	
		<title>
			로그인
		</title>
		<style>
			body{
				padding-left:5%;
				padding-right:5%;
				padding-top:0%;
								
			}
			
			.header{
				text-align:center; 
			}
			
			.footer{
				display:flex;
				justify-content:center;
				align-items:center;
				height:15%;
			
			}
			
			.mainpage{
				padding-left:200px;
				padding-right:200px;

			}
		
			.login_style{
				display:block;
				text-align:center;
				background-color:none;
				margin-top:20px
			}
			<!--로그인 창-->
			.login{
				display:flex;
				justify-content:center;
				align-items:center;
				font-size:2em;
				background-color:yellow;
			}
			
			input[type="text"]{
				border-radius:2px;
				font-size:1.5em;
				border-color:gray;
				width:600px;
				height:3em;
				margin-botton:30px;
			}
			input[type="text"]:hover{
				background:#eee;
				
			}
			input[type="password"]{
				border-radius:2px;
				font-size:1.5em;
				border-color:gray;
				width:600px;
				height:3em;
			}
			input[type="password"]:hover{
				background:#eee;
				
			}
			button[type="button"]{
				font-size:3em;
				width:600;
				height:3em;
			background-color:#eee;
				border:#eee;
				
			}
			button[type="button"]:hover{
				background:skyblue;
				border:skyblue;
				cursor:pointer;
			}
			a{
				text-decoration:none;
				color:green;
				font-size:1.2em;
			}
			a:hover{
				text-decoration:underline;
				color:green;
				font-size:1.2em;
				text-align:center;
			}
			</style>
			<script src ="https://code.jquery.com/jquery-3.3.1.min.js"></script>
			<script>
				function login(){
					$.post(
						"http://101.101.218.121/project_mart_controller/loginCheck",
						{"id" :document.getElementById("id").value, "pw" : document.getElementById("pw").value },
						function(data){
							console.log(data);
							
							if(data != 0){ 
								history.go(-1);
								//document.location.href = "http://101.101.218.121/project_mart_controller/start";
							}
							else{ 
								alert("입력하신 정보와 일치하는 회원정보가 없습니다");
								document.getElementById("id").value="";
								document.getElementById("pw").value="";
								
							}
						}
					);
				}
			</script>
	</head>
	<body>
		<p>
			<div class="header">
				<p><span style="font-size:3em;background-color:none;"><a href="http://101.101.218.121/project_mart_controller/start" style="text-decoration:none;color:black"><strong>커피장사</strong></a></span></p>
				<span style="font-size:1em;background-color:none;padding-right:10%;padding-left:10%; padding-top:0.3%;padding-botton:0.3%">
					커피 맛과 향, 커피장수에 의해 결정되노라
				</span>
			</div>
		</p>
		<hr>
		<br><br>
	
			<div style="text-align:center;font-size:2.7em">로그인<br>
			</div><br>
			
		<div class="mainpage">
			<hr>

		<!--본격 로그인 하는 창-->
		<div class ="login_style" style="margin-top:20px">
					<ul><input type="text" placeholder=" 아이디를 입력하세요" id="id" name="id" required></ul>
					<ul><input type="password" placeholder=" 비밀번호를 입력하세요" id="pw" name="pw" required></ul>
					<ul><button type="button" onclick="login();" style="font-size:1.2em;" value="로그인">로그인</button></ul>
		
				<p>
					<ul><a href="http://101.101.218.121/project_mart_controller/join">회원가입</a></ul>
					<span style="color:green;font-size:1.2em;"> </span>
					<ul><a href="#">아이디/비밀번호찾기</a></ul>

				</p>
			</div>
		</div>
		
		<script integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	
	</body>
</html>