﻿<!--모델에서 검사하기-->
<html>
	<head>
		<title>	
			회원가입
		</title>


		<style>
			body{
				padding-left:5%;
				padding-right:5%;
				padding-top:0%;
								
			}
			
			.header{
				text-align:center; 
			}
			
			.footer{
				display:flex;
				justify-content:center;
				align-items:center;
				height:15%;
			
			}
			
			.mainpage{
				padding-left:200px;
				padding-right:200px;

			}
			<!--버튼 스타일 만들기-->
			.alink{
			color:BLACK; <!--보이는 색깔-->
			text-decoration:none;
			}
			.join2_font{
				font-size:1.2em;
				padding-left:150px;
				padding-right:100px;
			}
			.join2_mainpage{
				padding-left:20px;
				padding-right:20px;
				
			}
			
			input[type=text]{
				border-radius:5px;
			}
			
			input[type=text]:hovor{
				background:aliceblue;
			}
			
			
			input[type=password]{
				border-radius:5px;
			}
			
			input[type=password]:hovor{
				background:aliceblue;
			}
			input[type=submit]{
				background-color:skyblue;
				width:200px;
				height:50px;
				font-size:1.5em;
				text-align:center;
				border-radius:5px;
				border-color:skyblue;
				
				
			}
			input[type=submit]:hover{
				background-color:orange;
				border-color:orange;
			
				
			}
		</style>
		
		<script>
		<!--비밀번호가 맞는지 입력 pw check_pw-->
		function pw_check(){
			if(document.getElementById("pw").value==document.getElementById("check_pw").value)
				return true;
			else{
				document.getElementById("ifwrong").innerHTML="*비밀번호가 일치하지 않습니다<br>";
				alert("비밀번호를 확인해주세요");
				return false;
			}
		}
		</script>
	</head>
	<body>
	
		<p>
			<div class="header">
				<p><span style="font-size:3em;background-color:none;"><a href="http://101.101.218.121/project_mart_controller/start" style="text-decoration:none;color:black"><strong>커피장사</strong></a></span></p>
				<span style="font-size:1em;background-color:none;padding-right:10%;padding-left:10%; padding-top:0.3%;padding-botton:0.3%">
					커피 맛과 향, 커피장수에 의해 결정되노라
				</span>
			</div>
		</p>
		<hr>
		<br><br>
	
			<div style="text-align:center;font-size:1.9em;">회원가입<br>
			</div><br>
			
		<form class="mainpage" action="http://101.101.218.121/project_mart_controller/join3" method="post" onsubmit="return pw_check();">

		<hr>
			<p class="join2_font">
				이 름 <br><input type="text" pattern="[가-힣]*" minlength=2 maxlength=5 name="name" id="name" required style="font-size:1.2em"><br>
				<br>
				
				아이디 <br><input type="text" pattern="[a-zA-z0-9]*" minlength=5 maxlength=10 name="id" id="id" required style="font-size:1.2em"><br>
				<span style="font-size:0.8em">(주의: 대소문자 알파벳, 숫자, 5~10자 사이로 입력하세요)</span><br>
				<br>
				
				비밀번호 <br><input type="password" pattern="[a-zA-z0-9~!@#$%^?&*()]*" minlength=5 maxlength=10 name="pw" id="pw" required style="font-size:1.2em"><br>
				<span style="font-size:0.8em">(주의: 대소문자 알파벳, 숫자,특수문자:~!@#$%^&*(), 5~10자 사이로 입력하세요)</span><br><br>
				비밀번호 재입력 <br><input type="password" pattern="[a-zA-z0-9~!@#$%^?&*()]*" minlength=5 maxlength=10 name="pw" id="check_pw" required style="font-size:1.2em"><br>
				<span id="ifwrong" style="color:red;font-size:1em"></span><!--틀리면 나타낼 글자-->
				<br>
				
				
				
				성 별 <select name="gender" id="gender" style="font-size:1em">
					<option value ="남자">남자</option>
					<option value="여자">여자</option>
					</select><br>
				<br>	
				
				전화번호 <br><input type="text" pattern ="^\d{3}-\d{3,4}-\d{4}$" name="phone_number" id="phone_number" required style="font-size:1.2em" placeholder="010-0000-0000"><br>
				<span style="font-size:0.8em">(주의: 010-0000-0000의 형태로 입력하세요)</span><br>
				<br>
				
				이메일 <br><input type="text" pattern="^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$" name="email" id="email" required style="font-size:1.2em"><br>
				<br>
				
				<div style="text-align:right"><input type="submit" value="다음"></div>
			</p>
			
			</form>
	</body>
</head>
</html>