﻿<html>
	<head>
		<title>
			퍼스널 옵션선택하기
			</title>
	
	
		<script src ="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script>
		var storage=1;
		var add_price=0;  //휘핑추가가격
		var price=0;  //shot가격
		var whipping_price=0;
		function plus(){
			if(storage<3){
				document.getElementById("n").innerHTML=storage+1;
				storage++;
				//window.opener.document.getElementById("add_shot").value=storage;//부모id=add_shot에 값전달하기
				document.getElementById("add_shot").innerHTML=500*storage;
				
				//price=Number(document.getElementById("price").value);
				//document.getElementById("price").value+=500;
				//document.getElementById("price").value=Number(document.getElementById("price").value);
				//document.getElementById("price").value+=500;
				price+=500;
				
				window.opener.document.getElementById("add_shot").value=storage;//부모id=add_shot에 값전달하기
			}
		}
		
		function minus(){
			if(storage!=1){
				document.getElementById("n").innerHTML=storage-1;
				storage--;
				document.getElementById("price").innerHTML=500*storage;
				//document.getElementById("price").value=Number(document.getElementById("price").value);
				price-=500;
				window.opener.document.getElementById("add_shot").value=storage;
			}
		}
			
		
		 // 라디오버튼 클릭시 이벤트 발생
		$(document).ready(function(){
		$("input:radio[name='whipping']").click(function(){
			if($("input[name='whipping']:checked").val() == "whipping_0"){
				document.getElementById("whipping_price").innerHTML=0;
				//document.getElementById("whipping_price").value=0;
				whipping_price=0;
				window.opener.document.getElementById("whipping").value="휘핑없음";
			}
			if($("input[name='whipping']:checked").val() == "whipping_1"){
				document.getElementById("whipping_price").innerHTML=500;
				//document.getElementById("whipping_price").value=500;
				whipping_price=500;
				window.opener.document.getElementById("whipping").value="휘핑 적게";
			}
			if($("input[name='whipping']:checked").val() == "whipping_2"){
				document.getElementById("whipping_price").innerHTML=500;
				//document.getElementById("whipping_price").value=500;
				whipping_price=500;
				window.opener.document.getElementById("whipping").value="휘핑많이";
				
            }
       
		   });
		});
		
		$(document).ready(function(){
		$("input:radio[name='ice']").click(function(){
			if($("input[name='ice']:checked").val() == "ice_0"){
				window.opener.document.getElementById("ice").value="얼음량 기본";
			}
			if($("input[name='ice']:checked").val() == "ice_1"){
				window.opener.document.getElementById("ice").value="얼음량 적게";
			}
			if($("input[name='ice']:checked").val() == "ice_2"){
				window.opener.document.getElementById("ice").value="얼음량 많이";
            }
       
		   });
		});

		$(document).ready(function(){
		$("input:radio[name='water']").click(function(){
			if($("input[name='water']:checked").val() == "water_0"){
				window.opener.document.getElementById("water").value="물량 기본";
			}
			if($("input[name='water']:checked").val() == "water_1"){
				window.opener.document.getElementById("water").value="물량 적게";
			}
			if($("input[name='water']:checked").val() == "water_2"){
				window.opener.document.getElementById("water").value="물량 많이";
            }
       
		   });
		});
		
		function add_price1(){
			
			add_price=whipping_price+price;  //옵션추가가격
			var win_add_price=Number(window.opener.document.getElementById("add_price").value);
			win_add_price=add_price;//부모창의 id=add_price에 추가금액가격 대입
			var win_total_price=Number(window.opener.document.getElementById("total_price").value);
			win_total_price+=add_price;
			window.opener.document.getElementById("total_price").value=win_total_price;
			window.opener.document.getElementById("total_price").innerHTML=win_total_price;
		
			window.close();
		}
		
	
		</script>
		</head>
	<body>

		<input type ="hidden" name="옵션추가 금액" id="price" value="0">
		샷추가(<span id="add_shot">0</span>)원<br>
			<span><a href="javascript:minus();" style="background-color:skyblue;padding-left:10pt;padding-right:10pt;font-size:1em;">-</a></span>
			<span id="n">1</span>
			<span><a href="javascript:plus();"  style="background-color:skyblue;padding-left:10pt;padding-right:10pt;font-size:1em;">+</a></span>
		<br>
		
		얼음(<span id="add_ice">0</span>)원<br>
			<input type ="radio" name="ice" value="ice_0" checked>
			<span>기본</span>
			
			<input type ="radio" name="ice" value="ice_1">
			<span>적게</span>
			
			<input type ="radio" name="ice" value="ice_2">
			<span>많이</span>
			<br>
		베이스 - 물(<span id="add_water">0</span>)원<br>
			<input type ="radio" name="water" value="water_0" checked>
			<span>기본</span>
			<input type ="radio" name="water" value="water_1">
			<span>적게</span>
			
			<input type ="radio" name="water" value="water_2">
			<span>많이</span>
		<br>
		휘핑크림(<span id="whipping_price">0</span>)원<br>
			<input type ="radio" name="whipping" value="whipping_0" checked>
			<span>없음</span>
		
			<input type ="radio" name="whipping" value="whipping_1">
			<span>적게</span>
			
			<input type ="radio" name="whipping" value="whipping_2">
			<span>많이</span>
		<br>
		
		<button type="button" name="button" onclick='add_price1();'>적용하기</button>


	</body>
</html>