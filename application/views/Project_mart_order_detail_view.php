﻿<html>
	<head>	
		<title>
			로그인
		</title>
		<style>
			body{
				padding-left:5%;
				padding-right:5%;
				padding-top:0%;
								
			}
			
			.header{
				text-align:center; 
			}
			
			.footer{
				display:flex;
				justify-content:center;
				align-items:center;
				height:15%;
			
			}
			
			.mainpage{
				padding-left:200px;
				padding-right:200px;

			}
		
			.login_style{
				display:block;
				text-align:center;
				background-color:none;
				margin-top:20px
			}
			<!--로그인 창-->
			.login{
				display:flex;
				justify-content:center;
				align-items:center;
				font-size:2em;
				background-color:yellow;
			}
			
			input[type="text"]{
				border-radius:2px;
				font-size:1.5em;
				border-color:gray;
				width:600px;
				height:3em;
				margin-botton:30px;
			}
			input[type="text"]:hover{
				background:#eee;
				
			}
			input[type="password"]{
				border-radius:2px;
				font-size:1.5em;
				border-color:gray;
				width:600px;
				height:3em;
			}
			input[type="password"]:hover{
				background:#eee;
				
			}
			button[type="button"]{
				font-size:3em;
				width:600;
				height:3em;
			background-color:#eee;
				border:#eee;
				
			}
			button[type="button"]:hover{
				background:skyblue;
				border:skyblue;
				cursor:pointer;
			}
			a{
				text-decoration:none;
				color:green;
				font-size:1.2em;
			}
			a:hover{
				text-decoration:underline;
				color:green;
				font-size:1.2em;
				text-align:center;
			}
			
			.join2_font{
				font-size:1.2em;
				padding-left:150px;
				padding-right:100px;
			}
			</style>
		<script src ="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script>
		function copy(){
			var chk = $("input:checkbox[name='checkbox']").is(":checked");
		
			if(chk){
				document.getElementById("re_name").value=document.getElementById("name").value;
				document.getElementById("re_phone_number").value=document.getElementById("phone_number").value;
				document.getElementById("re_adress").value=document.getElementById("adress").value;
				document.getElementById("re_email").value=document.getElementById("email").value;
			}
		}
		</script>
	</head>
	<body>
		<p>
			<div class="header">
				<p><span style="font-size:3em;background-color:none;"><a href="http://101.101.218.121/project_mart_controller/start" style="text-decoration:none;color:black"><strong>커피장사</strong></a></span></p>
				<span style="font-size:1em;background-color:none;padding-right:10%;padding-left:10%; padding-top:0.3%;padding-botton:0.3%">
					커피 맛과 향, 커피장수에 의해 결정되노라
				</span>
			</div>
		</p>
		<hr>
		<br><br>
		<form action ="http://101.101.218.121/project_mart_controller/finish_order_detail_view" method="post">
			<div style="text-align:center;font-size:2.7em">주문자 정보 입력<br>
			</div><br>
			<div class="mainpage" style="text-align:left"><hr>
				<p class="join2_font">
				<div style="font-size:2em;">주문자 정보</div>
				이 름 <br><input type="text" pattern="[가-힣]*" minlength=2 maxlength=5 name="name" id="name" required style="font-size:1.2em"><br>
				<br>
				
				전화번호 <br><input type="text" pattern ="^\d{3}-\d{3,4}-\d{4}$" name="phone_number" id="phone_number" required style="font-size:1.2em" placeholder="010-0000-0000"><br>
				<span style="font-size:0.8em">(주의: 010-0000-0000의 형태로 입력하세요)</span><br>
				<br>
				
				주소 <br><input type="text" minlength=5 maxlength=20 name="adress" id="adress" required style="font-size:1.2em"><br>
				<span style="font-size:0.8em">(주의:세부주소까지 정확하게 입력하세요)</span><br>
				<br>
			
				이메일 <br><input type="text" pattern="^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$" name="email" id="email" required style="font-size:1.2em"><br>
				<br>
				
			</p>
			
			
			<p class="join2_font">
				<div style="font-size:2em;">받는 사람 정보</div>
				<div style="font-size:1.5em"><input type="checkbox" name="checkbox" onclick="copy()">위 정보와 동일</div>
				이 름 <br><input type="text" pattern="[가-힣]*" minlength=2 maxlength=5 name="re_name" id="re_name" required style="font-size:1.2em"><br>
				<br>
				
				전화번호 <br><input type="text" pattern ="^\d{3}-\d{3,4}-\d{4}$" name="re_phone_number" id="re_phone_number" required style="font-size:1.2em" placeholder="010-0000-0000"><br>
				<span style="font-size:0.8em">(주의: 010-0000-0000의 형태로 입력하세요)</span><br>
				<br>
				
				주소 <br><input type="text" minlength=5 maxlength=20 name="re_adress" id="re_adress" required style="font-size:1.2em"><br>
				<span style="font-size:0.8em">(주의:세부주소까지 정확하게 입력하세요)</span><br>
				<br>
			
			
				이메일 <br><input type="text" pattern="^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$" name="re_email" id="re_email" required style="font-size:1.2em"><br>
				<br>
				
				<div style="text-align:left"><input type="submit" value="결제하기"></div>
			</p>
		</form>
	</body>
</html>