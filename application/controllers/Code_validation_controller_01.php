﻿<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Code_validation_controller_01 extends CI_controller {
 
	 public function check(){

		$this->load->library("form_validation");
		$this->form_validation->set_rules("name","이름","required|max_length[10]|min_length[2]");
		$this->form_validation->set_rules("phone_number","폰넘버","numeric|required");
		$this->form_validation->set_rules("pw","비번","required|min_length[6]");
		$this->form_validation->set_rules("gender","젠더","required|in_list[여자,남자]");
		
		
		if($this->form_validation->run()){
			echo "정확하게 입력";
			
			$this->load->model("Code_validatioin_model");
			if($this->code_validation_model->check())
				echo "회원가입이 완료되었습니다.";
			
			else echo "알 수 없는 오류가 발생하였습니다";
			
		}
		
		else {
			echo "적합하지 않은 입력";
		}
	 }
}


?>