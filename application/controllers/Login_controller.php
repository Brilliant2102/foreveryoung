﻿ <!--echo를 view에서 출력하도록 바꾸기-->
<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Login_controller extends CI_Controller 
{
 
    public function login()
    {

		  $this->load->library( "session" );
		  $this->load->model( "Login_model" );
		  $result = $this->Login_model->login_check( $_POST["id"], $_POST["pw"] );
		  
		  if( $result->num_rows() != 1 ) echo "로그인 실패입니다.";
		  
		  else {
		   $this->session->set_userdata( "id", $_POST["id"] );
		  echo "로그인 성공입니다.";
		  }
	
	  
	  
	}
 
}
 
?>