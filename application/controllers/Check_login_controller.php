﻿ <!--echo를 view에서 출력하도록 바꾸기-->
<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Check_login_controller extends CI_Controller 
{
 
    public function login()
    {

		  $this->load->library( "session" );
		  $this->load->model( "Login_model" );
		  $result = $this->Login_model->login_check( $_POST["id"], $_POST["pw"] );
		  
		  if( $result->num_rows() != 1 ) echo "본인확인에 실패했습니다";
		  
		  else {
		   $this->session->set_userdata( "id", $_POST["id"] );
		   
		   $data=array(
				"ID_IN"=$_POST["id"]
		   );
		   $this->load->view("Change02_view",$data);
		  }
	
	  
	  
	}
 
}
 
?>